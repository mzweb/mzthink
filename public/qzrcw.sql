/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : qzrcw

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-11-08 21:08:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `mz_article`
-- ----------------------------
DROP TABLE IF EXISTS `mz_article`;
CREATE TABLE `mz_article` (
  `art_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name_cn` varchar(60) NOT NULL COMMENT '文章cn',
  `name_en` varchar(60) NOT NULL COMMENT '文章en',
  `img` varchar(255) NOT NULL COMMENT '缩略图',
  `keywords` varchar(100) NOT NULL COMMENT '文章关键词',
  `description` varchar(255) NOT NULL COMMENT '文章简介',
  `content` text NOT NULL COMMENT '文章内容',
  `add_time` int(11) NOT NULL COMMENT '发布时间',
  `type` varchar(100) NOT NULL DEFAULT '3' COMMENT '1:置顶; 2:首页推荐; 3:显示; 4:部分可见;',
  `template` varchar(30) NOT NULL COMMENT '使用的模板名称',
  `model_id` mediumint(8) unsigned DEFAULT NULL COMMENT '模型名称',
  `sort` smallint(5) unsigned DEFAULT '50' COMMENT '文章权重',
  `status` tinyint(3) unsigned DEFAULT '1' COMMENT '1:正常; 0:回收站',
  PRIMARY KEY (`art_id`),
  UNIQUE KEY `unique_enname` (`name_en`),
  UNIQUE KEY `unique_cnname` (`name_cn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mz_article
-- ----------------------------

-- ----------------------------
-- Table structure for `mz_article_model`
-- ----------------------------
DROP TABLE IF EXISTS `mz_article_model`;
CREATE TABLE `mz_article_model` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL COMMENT '模型名称',
  `status` tinyint(3) unsigned DEFAULT '1' COMMENT '1:正常;0:回收站',
  `sort` smallint(5) unsigned DEFAULT '50' COMMENT '模型权重',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mz_article_model
-- ----------------------------
INSERT INTO `mz_article_model` VALUES ('18', '文章', '1', '51');
INSERT INTO `mz_article_model` VALUES ('19', '招聘', '1', '50');
INSERT INTO `mz_article_model` VALUES ('20', '简历', '1', '50');
INSERT INTO `mz_article_model` VALUES ('21', '企业', '1', '50');
INSERT INTO `mz_article_model` VALUES ('22', '个人', '1', '50');

-- ----------------------------
-- Table structure for `mz_article_model_val`
-- ----------------------------
DROP TABLE IF EXISTS `mz_article_model_val`;
CREATE TABLE `mz_article_model_val` (
  `val_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `model_id` mediumint(9) NOT NULL DEFAULT '0',
  `name_cn` varchar(60) NOT NULL COMMENT '值名称cn',
  `name_en` varchar(60) NOT NULL COMMENT '值名称en',
  `type` tinyint(3) unsigned DEFAULT NULL COMMENT '1:文本; 2:单选; 3:复选; 4:下拉; 5:文本域; 6:附件;',
  `required` tinyint(3) unsigned DEFAULT '0' COMMENT '0:可选; 1:必填',
  `default_val` varchar(100) NOT NULL COMMENT '默认值',
  `value` varchar(255) NOT NULL COMMENT '可选值',
  `status` tinyint(3) unsigned DEFAULT '1' COMMENT '1:正常;0:回收站',
  `sort` smallint(5) unsigned DEFAULT '50' COMMENT '字段权重',
  PRIMARY KEY (`val_id`),
  UNIQUE KEY `unique_enname` (`name_en`),
  UNIQUE KEY `unique_cnname` (`name_cn`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mz_article_model_val
-- ----------------------------
INSERT INTO `mz_article_model_val` VALUES ('1', '18', '测试', 'test', '3', '1', '', '', '1', '50');

-- ----------------------------
-- Table structure for `mz_category`
-- ----------------------------
DROP TABLE IF EXISTS `mz_category`;
CREATE TABLE `mz_category` (
  `cate_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name_cn` varchar(60) NOT NULL COMMENT '值名称cn',
  `name_en` varchar(60) NOT NULL COMMENT '值名称en',
  `type` tinyint(3) unsigned DEFAULT NULL COMMENT '1:首页; 2:推荐',
  `sort` smallint(5) unsigned DEFAULT NULL COMMENT '模块权重',
  `status` tinyint(3) unsigned DEFAULT '1' COMMENT '1:正常;0:回收站',
  PRIMARY KEY (`cate_id`),
  UNIQUE KEY `unique_enname` (`name_en`),
  UNIQUE KEY `unique_cnname` (`name_cn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mz_category
-- ----------------------------
