<?php
/**
 * 助手函数拓展
 */

if(!function_exists('mzDb')){
	/**
	 * 读取数据库配置方法
	 * @param  [type] $database [description]
	 * @return [type]           [description]
	 */
	function mzDb($database='no'){
		@$data=include APP_DATA."../database.php";
		if(empty($data)){
			dd('数据库配置文件丢失！请将/application/database.back文件改为database.php');
		}
		if($database!='no'){	//自定义库名
			$data['database']=$database;
		}
		$dns="{$data['type']}:host:{$data['hostname']}; dbname={$data['database']}";
		$user=$data['username'];
		$pass=$data['password'];
		return new PDO($dns,$user,$pass,['ATTR_PERSISTENT'=>true]);
	}
}


if(!function_exists('dd')){
	/**
	 * 打印停止方法
	 * @param  [All] $data [要打印的变量]
	 * @param  string $type [要用什么方法打印]
	 * @return [no return]
	 */
	function dd($data,$type='dump',$die=true){
		echo "<pre>";
		switch($type){
			case 'dump':
				var_dump($data);
				break;
			case 'export':
				var_export($data);
				break;
		}
		if($die==true){
			die;
		}
	}
}

if(!function_exists('layui_msg')){	//提示性弹窗
	function layui_msg($txt){	
		echo '<script type="text/javascript" src="'.__STATIC__.'/lib/layui/layui.js" charset="utf-8"></script>';
		echo "
		<script>
			layui.use('layer', function(){
	  			var layer = layui.layer;
	  			layer.msg('{$txt}');
			});  
			
		</script>";
	}
}

if(!function_exists('layui_open')){	//open弹窗 详情见layui手册
	function layui_open($title="",$content="",$type='0'){	
		echo '<script type="text/javascript" src="'.__STATIC__.'/lib/layui/layui.js" charset="utf-8"></script>';
		echo "
		<script>
			layui.use('layer', function(){
	  			var layer = layui.layer;
	  			layer.open({
					type:	  {$type},
				 	title:	 '{$title}',
					content: '{$content}'
				});  
			});  
			
		</script>";
	}
}