<?php
namespace app\admin\behavior;
use mz\ControllerBase;

class Index extends ControllerBase{

	public function __construct(){	//请不要执行父类方法 否则报错

	}

	public function getInfo(&$param){	//获取信息
		$data['system'] = $this->getSystem();	//获取系统信息

		return $data;
	}

	private function getSystem(){	//获取系统信息


		$data = [
			'php_os'		=>	PHP_OS,
			'server_name'	=>	$_SERVER['SERVER_ADDR']
		];

		return $data;
	}
}