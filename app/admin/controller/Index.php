<?php
namespace app\admin\controller;
use think\Hook;

class Index extends Base{

	public function __construct(){
		parent::__construct();

	}

	public function index(){

		dd(config('default_ajax_return', 'json'));

		$data=Hook::exec('app\admin\behavior\Index','get_info');	//获取基础数据
		// 用法请见/app/base/ControllerBase

		$this->assign([
			'data'	=>	$data,
		]);

		return view();
		
	}

}