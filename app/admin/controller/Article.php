<?php
namespace app\admin\controller;
use app\admin\controller\Base;
use Exception;
class Article extends Base{
	public function _initialize(){
		parent::_initialize();
	}
	public function manage(){	//文章管理
		return view();
	}
	public function model(){	//文章模型管理(界面)
		//列表页面渲染
		$data=db()->query("select * from mz_article_model where `status` = 1 order by sort desc");
		$this->assign([
			'data'=>$data,
			'sum'=>count($data),
		]);
		return view();
	}
	public function model_add(){	//文章模型添加(界面)
		return view();
	}
	public function model_edit(){	//模型编辑操作(界面)
		$id=input('id');
		$data=db()->query("select * from mz_article_model where `id` = '$id' and `status` =1");
		if(empty($data)){
			ld("{$id} 该ID不存在或已被删除");
		}
		$this->assign([
			'data'=>$data[0],
		]);
		return view();
	}
	public function model_execute(){	//模型写操作
		if(request()->ispost()){	//模型添加 && 修改操作
			if(input('del_id/a')){	//删除操作
				$data=implode(',',input('del_id/a'));
				db()->query("update mz_article_model set status = 0 where id in($data) "); //移动至回收站
				// db()->query("delete from mz_article_model where id in($data)"); 
				return $data;
			}
			$data=[
				'name'=>input('name'),
				'sort'=>input('sort'),
			];
			try{
				if($id=input('id')){ //修改操作
					$send=db()->query("select * from mz_article_model where `name` = '{$data['name']}' and `id` != '{$id}'");
					if(empty($send)){
						$send2=db()->query("update mz_article_model set `name` = '{$data['name']}', `sort` = '{$data['sort']}' where `id` = {$id}");
						ld('更新模型成功');
					}else{
						throw new Exception();
					}
				}else{	//新增操作
					$send=db()->query("select * from mz_article_model where name='{$data['name']}'");
					if(empty($send)){

						$send2=db()->query("insert into mz_article_model (`name`,`sort`) values ('{$data['name']}','{$data['sort']}')");
						ld('添加模型成功');
					}else{
						throw new Exception();
					}
				}
			}catch (Exception $e){
				ld('请修改模型名称,该模型已存在,操作失败!');
			}
			
		}
	}

	public function model_val(){	//模型字段控制(界面)
		$id=input('model_id');
	 	$model=db()->query("select * from mz_article_model where id = {$id}");
	 	$data=db()->query("select * from mz_article_model_val where `model_id` = '{$id}' and `status` = 1");
	 	foreach($data as $k=>$v){
	 		switch($v['type']){
	 			case 1:
	 				$data[$k]['type']='单行文本';
	 				break;
	 			case 2:
	 				$data[$k]['type']='单选框';
	 				break;
	 			case 3:
	 				$data[$k]['type']='复选框';
	 				break;
	 			case 4:
	 				$data[$k]['type']='下拉框';
	 				break;
	 			case 5:
	 				$data[$k]['type']='文本框';
	 				break;
	 			case 6:
	 				$data[$k]['type']='附件';
	 				break;
	 		}
	 	}
		$this->assign([
			'data'=>$data,	//模型字段数据列表
			'sum'=>count($data),	//模型字段数据列表
			'model'=>$model[0],	//所属模型信息
		]);
		return view();
	}
	public function model_val_add(){	//模型字段添加(界面)
		$model_id=input('model_id');
		$this->assign([
			'model_id'=>$model_id,
		]);
		return view();
	}
	public function model_val_edit(){	//模型字段编辑操作(界面)
		$val_id=input('val_id');
		$data=db()->query("select * from mz_article_model_val where `val_id` = '$val_id' and `status` =1");
		if(empty($data)){
			ld("{$id} 该ID不存在或已被删除");
		}
		$this->assign([
			'data'=>$data[0],
		]);
		return view();
	}
	public function model_val_execute(){	//模型字段写操作
		$data=input('post.');
		if(input('del_id/a')){	//删除
			$data=implode(',',input('del_id/a'));
			db()->query("update mz_article_model_val set status = 0 where val_id in($data) "); //移动至回收站
			// db()->query("delete from mz_article_model where id in($data)"); 
			return $data;
		}
		if(!isset($data['val_id'])){	//新增
			$send=db()->query("select `model_id` from mz_article_model_val where `name_en` = '{$data['name_en']}' or `name_cn` = '{$data['name_cn']}'");
			if($send){
				ld('请修改模型字段名称,该模型字段已存在,操作失败!');
			}
			$sql="insert into mz_article_model_val (`model_id`,`name_cn`,`name_en`,`type`,`required`,`default_val`,`value`,`sort`) values ('{$data['model_id']}','{$data['name_cn']}','{$data['name_en']}','{$data['type']}','{$data['required']}','{$data['default_val']}','{$data['value']}','{$data['sort']}')";
			db()->query($sql);
			ld('添加模型字段成功');
		}else{	//修改
			$send=db()->query("select * from mz_article_model_val where `name_cn` = '{$data['name_cn']}' and `val_id` != '{$data['val_id']}' or `name_en` = '{$data['name_en']}' and `val_id` != '{$data['val_id']}'");
			if(empty($send)){
				$send2=db()->query("update mz_article_model_val set `name_cn` = '{$data['name_cn']}', `name_en` = '{$data['name_en']}', `type` = '{$data['type']}', `required` = '{$data['required']}', `sort` = '{$data['sort']}', `default_val` = '{$data['default_val']}', `value` = '{$data['value']}' where `val_id` = {$data['val_id']}");
				ld('更新模型成功');
			}else{
				ld('请修改模型字段名称,该模型字段已存在,操作失败!');
			}
		}
		
	}
}