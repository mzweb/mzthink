<?php
namespace app\admin\controller;
use mz\ControllerBase;
use app\admin\model\Article;
/**
 * 后台基础类
 * qq: 54600488
 * author: mz
 * 如对代码有任何疑问可联系作者
 */
class Base extends ControllerBase{
	public function __construct(){
		parent::__construct();
		


		//进行权限认证
		

		$info = include APP_DATA . "info.php";	//获取软件基础信息
		
		$nav = include APP_DATA . "nav.php";	//获取公共导航

		$this->assign([
			'nav'=>$nav,
			'info'=>$info,
		]);
		
	}
    
}