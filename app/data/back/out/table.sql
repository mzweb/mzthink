
alter table mz_article_model add `sort` smallint unsigned default '50' comment '模型权重'



##用户表
#=================
create table mz_user (

)engine=innodb default charset=utf8;






##文章表
#=================
create table mz_article(
`art_id` mediumint auto_increment,
`name_cn` varchar(60) not null comment '文章cn',
`name_en` varchar(60) not null comment '文章en',
`img` varchar(255) not null comment '缩略图',
`keywords` varchar(100) not null comment '文章关键词',
`description` varchar(255) not null comment '文章简介',
`content` text not null comment '文章内容',
`add_time` int not null comment '发布时间',
`type` varchar(100) not null default '3' comment '1:置顶; 2:首页推荐; 3:显示; 4:部分可见;',
`template` varchar(30) not null comment '使用的模板名称',
`model_id` mediumint unsigned comment '模型名称',
`sort` smallint unsigned default '50' comment '文章权重',
`status` tinyint unsigned default '1' comment '1:正常; 0:回收站',
primary key(`art_id`),unique unique_enname (`name_en`),unique unique_cnname (`name_cn`)
)engine=innodb default charset=utf8;


##模型表
#=================
create table mz_article_model(
`id` mediumint auto_increment,
`name` varchar(60) not null comment '模型名称',
`sort` smallint unsigned default '50' comment '模型权重',
`status` tinyint unsigned default '1' comment '1:正常;0:回收站',
primary key(`id`),unique unique_name(`name`)
)engine=innodb default charset=utf8;

##模型值表
#=================
create table mz_article_model_val(
`val_id` mediumint auto_increment,
`model_id` mediumint,
`name_cn` varchar(60) not null comment '值名称cn',
`name_en` varchar(60) not null comment '值名称en',
`type` tinyint unsigned comment '1:文本; 2:单选; 3:复选; 4:下拉; 5:文本域; 6:附件;',
`required` tinyint unsigned default '0' comment '0:可选; 1:必填',
`default_val` varchar(100) not null comment '默认值',
`value` varchar(255) not null comment '可选值',
`sort` smallint unsigned default '50' comment '字段值权重',
`status` tinyint unsigned default '1' comment '1:正常;0:回收站',
primary key(`val_id`),unique unique_enname(`name_en`),unique unique_cnname(`name_cn`)
)engine=innodb default charset=utf8;

##模块表
#=================
create table mz_category(
`cate_id` mediumint auto_increment,
`name_cn` varchar(60) not null comment '值名称cn',
`name_en` varchar(60) not null comment '值名称en',
`type` tinyint unsigned comment '1:首页; 2:推荐',
`sort` smallint unsigned default '50' comment '模块权重',
`status` tinyint unsigned default '1' comment '1:正常;0:回收站',
primary key(`cate_id`),unique unique_enname(`name_en`),unique unique_cnname(`name_cn`)
)engine=innodb default charset=utf8;
