create table mz_config(
	`conf_id` mediumint auto_increment,
	`conf_en` varchar(60) not null,
	`conf_cn` varchar(60) not null,
	`conf_value` varchar(255) comment '配置可选值',
	`conf_value_default` varchar(60) comment '配置默认值(配合可选值使用)',
	`conf_style` tinyint unsigned default '0' comment '配置类型{0:单行文本,1:单选框,2:复选框,3:下拉框,4:文本域,5:文件}',
	`conf_tab` smallint unsigned comment '配置所属选项卡',
	`conf_status` tinyint unsigned default '1' comment '配置状态{0:禁用,1:启用}',
	`conf_sort` tinyint unsigned default '50',
	primary key(`conf_id`)
)engine=innodb default charset=utf8;

create table mz_config_tab(
	`tab_id` smallint auto_increment,
	`tab_name` varchar(60) not null,
	`tab_sort` tinyint unsigned default '50' comment '选项卡删除后会将其中的配置全部移动到sort值最小的选项卡',
	primary key(`tab_id`)
)engine=innodb default charset=utf8;
