<?php
/**
 * 导航配置
 */


//返回导航数据
return [
	['title'=>'系统设置',
		'iconic'=>'&#xe696;',
		'data'=>[
			['src'=>'/admin/config/tab','name'=>'选项卡管理'],
			['src'=>'/admin/config/variable','name'=>'变量管理'],
		],
	],

	['title'=>'权限管理',
		'iconic'=>'&#xe6ae;',
		'data'=>[
			['src'=>'/admin/auth/admin','name'=>'管理员管理'],
			['src'=>'/admin/auth/role','name'=>'角色管理'],
			['src'=>'/admin/auth/power','name'=>'权限管理'],
		]
	],

	['title'=>'api',
		'iconic'=>'&#xe6db;',
		'data'=>[
			['src'=>'/admin/auth/admin','name'=>'已安装的api'],
			['src'=>'/admin/auth/role','name'=>'api拓展下载'],
		]
	],

	//后期拓展 先不写
	['title'=>'模块管理',
		'iconic'=>'&#xe6b5;',
		'data'=>[
			['src'=>'/admin/module/index','name'=>'模块管理'],
			['src'=>'/admin/model/index','name'=>'模型管理'],
			['src'=>'/admin/example/index','name'=>'实例管理'],
			['src'=>'/admin/recycle/index','name'=>'回收站'],
		]
	],

	['title'=>'帮助',
		'iconic'=>'&#xe705;',
		'data'=>[
			['src'=>'/admin/auth/admin','name'=>'手册'],
			['src'=>'/admin/auth/role','name'=>'作者'],
			['src'=>'/admin/auth/power','name'=>'官网'],
		]
	],

	['title'=>'回收站',
		'iconic'=>'&#xe726;',
		'data'=>[
				['src'=>'/admin/auth/power','name'=>'回收站管理'],
			],
	],

	['title'=>'用户管理',
		'iconic'=>'&#xe726;',
		'data'=>[
				['src'=>'/admin/auth/power','name'=>'用户管理'],
			],
	],
	
];