<?php
namespace mz;
use think\Config;
//无强制要求继承任何类

class Behavior{
	public function initGlobalInfo(&$param){	//声明公用常量(含视图常量)
		$Info = Config::get("view_replace_str");	//读取原配置文件

        $Global = include APP_BASE."Global.php";	//读取公开常量库

        $Info = array_merge($Info,$Global);	//合并

        Config::set("view_replace_str",$Info);	//设置视图常量

        foreach($Global as $k=>$v){	//定义常量
            define($k,$v);
        }

        include APP_BASE."Common.php";  //声明函数 常量
	}
}