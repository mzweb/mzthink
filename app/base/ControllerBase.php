<?php
namespace mz;
use think\Controller;
use think\Hook;
/**
 * 通用基础类
 * qq: 54600488
 * author: mz
 * 如对代码有任何疑问可联系作者
 */
class ControllerBase extends Controller{

	public function __construct(){
        parent::__construct();

        // 初始化请求信息
        $this->initRequestInfo();

        Hook::exec('\mz\Behavior','init_global_info');  //声明公用常量(含视图常量)
        // $param='Welcome to use MzThink!';
        // 默认行为范例 默认行为调用run方法
        // Hook::add('app_init','\mz\Behavior');    //声明
        // Hook::listen('app_init',$param); //调用
        // Hook::exec('\mz\Behavior','fun_name',$param);    //直接执行 
        // fun_name将会调用funName方法   $parent必须为变量
        // 可以自定义创建行为文件
        
        
	}

     /**
     * 初始化请求信息
     */
    final private function initRequestInfo()
    {
        
        defined('IS_POST')          or define('IS_POST',         $this->request->isPost());
        defined('IS_GET')           or define('IS_GET',          $this->request->isGet());
        defined('IS_AJAX')          or define('IS_AJAX',         $this->request->isAjax());
        defined('IS_PJAX')          or define('IS_PJAX',         $this->request->isPjax());
        defined('IS_MOBILE')        or define('IS_MOBILE',       $this->request->isMobile());
        defined('MODULE_NAME')      or define('MODULE_NAME',     strtolower($this->request->module()));
        defined('CONTROLLER_NAME')  or define('CONTROLLER_NAME', strtolower($this->request->controller()));
        defined('ACTION_NAME')      or define('ACTION_NAME',     strtolower($this->request->action()));
        // defined('URL')              or define('URL',             CONTROLLER_NAME . SYS_DS_PROS . ACTION_NAME);
        defined('URL_TRUE')         or define('URL_TRUE',        $this->request->url(true));
        defined('DOMAIN')           or define('DOMAIN',          $this->request->domain());
        defined('URL_ROOT')         or define('URL_ROOT',        $this->request->root());

        $this->param = $this->request->param();
    }

    /**
     * 获取实例
     */
	public function __get($name){

        // 自动加载资源
        $str = 'loader_';
        if(strpos($name, $str) === 0){
            $object = explode('_',str_replace($str,'',$name));
            if(count($object) == 2){
                return model($object[1],$object[0]);
            }
        }

    	if($name == 'db'){   //获取原生PDO连接资源
    	   $this->db = mzDb();
           return $this->db;
    	}

    }

    

}
