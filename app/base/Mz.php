<?php
namespace mz;

/**
 * 用法：
 * load_trait('controller/Mz');
 * class index
 * {
 *     use mz\Mz;
 *     public function index(){
 *         $this->error();
 *         $this->redirect();
 *     }
 * }
 */
trait Mz{

	/**
	 * 跳转方法带样式
	 * @param  [string] $txt [提示文字]
	 * @param  string $url [将跳转的URL]
	 * @return [no return]
	 */
	public function Jump($txt,$url='',$time='2'){	
		layui_msg($txt);
		if(empty($url)){
			if(isset($_SERVER['HTTP_REFERER'])){
				$url=$_SERVER['HTTP_REFERER'];
			}else{
				$url=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'];
			}
		}
		header("Refresh: $time; url=$url"); //延迟转向 也就是隔几秒跳转
	}

	/**
	 * 专门的api函数
	 * @param [type] $data    [数据]
	 * @param string $code    [状态码]
	 * @param string $message [提示语]
	 */
	public function ApiJson($data,$code='0',$message=''){
		json_encode([
			'Data'		=>	$data,
			'Code'		=>	$code,
			'Message'	=>	$message
		],320);
	}
}
}